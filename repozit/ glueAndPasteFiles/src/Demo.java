import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Demo {
    public static void main(String[] args) throws IOException {
        String path = "D:\\1\\file.txt";
        ArrayList<File> list = new ArrayList<>();
        File first = new File("D:\\1\\first");
        File second = new File("D:\\1\\second");
        list.add(first);
        list.add(second);

        GlueAndDeGlueFile glueAndDeGlueFile = new GlueAndDeGlueFile(path);
        glueAndDeGlueFile.gluingFile(list);
        glueAndDeGlueFile.deGluingFile("D:\\1");
    }
}
