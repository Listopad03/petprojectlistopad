import java.io.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * class extends of the {@code File} class.
 * the class can merge several files into one and paste one file into several files
 */
public class GlueAndDeGlueFile extends File {
    private static final String ENCODING = "UTF-8";

    public GlueAndDeGlueFile(String pathname) {
        super(pathname);
    }
    /**
     * Method merges multiple files into one
     *
     * @param  fileArrayList array of files
     * @throws IOException if file not found or not accessible
     */
    public void gluingFile(ArrayList<File> fileArrayList) throws IOException {
        this.createNewFile();
        try (FileOutputStream fileOutputStream = new FileOutputStream(this, true)) {
            for (File file : fileArrayList) {
                byte[] bytePath = file.getAbsolutePath().getBytes();
                byte[] sizePath = ByteBuffer.allocate(4).putInt(bytePath.length).array();
                byte[] byteData = getBytesFileData(file);
                byte[] sizeData = ByteBuffer.allocate(4).putInt(byteData.length).array();
                fileOutputStream.write(sizePath);
                fileOutputStream.write(bytePath);
                fileOutputStream.write(sizeData);
                fileOutputStream.write(byteData);
            }
        }
    }

    /**
     * the method pastes one file, creates new ones with its data
     *
     * @param  pathToDirOut path to the directory in which the file will be created
     * @throws IOException if file not found or not accessible
     */
    public ArrayList<File> deGluingFile(String pathToDirOut) {
        ArrayList<File> arrayList = new ArrayList<>();
        try (BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(this))) {
            while (bufferedInputStream.available() > 0) {
                int sizePath = getIntFromByteArray(bufferedInputStream);
                String oldPath = getStringFromByteArray(bufferedInputStream, sizePath);
                String newPath = pathToDirOut + oldPath.substring(oldPath.lastIndexOf(File.separator));
                File file = new File(newPath);
                int sizeData = getIntFromByteArray(bufferedInputStream);
                createFileWithValue(file, getStringFromByteArray(bufferedInputStream, sizeData));
                arrayList.add(file);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    private void createFileWithValue(File file, String value) throws IOException {
        file.createNewFile();
        Writer outputStream = null;

        try {
            outputStream = new OutputStreamWriter(new FileOutputStream(file, true), ENCODING);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (outputStream != null) {
            try (BufferedWriter bufferedWriter = new BufferedWriter(outputStream)) {
                bufferedWriter.write(value);
            }
        }
    }

    private String getStringFromByteArray(BufferedInputStream bufferedInputStream, int size) throws IOException {
        byte[] bytes = bufferedInputStream.readNBytes(size);
        return new String(bytes);
    }

    private int getIntFromByteArray(BufferedInputStream bufferedInputStream) throws IOException {
        byte[] bytes = bufferedInputStream.readNBytes(4);
        return ByteBuffer.wrap(bytes).getInt();
    }

    private byte[] getBytesFileData(File file) {
        byte[] bytesData = null;
        try (BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file))) {
            bytesData = bufferedInputStream.readAllBytes();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bytesData;
    }
}
